# README #

These little scripts and tools are to small to justify a project of their own, but I wanted to save them somewhere!

## Scripts description? ##
###dz
Shortcut to launch dzen2 as a clock (edit position accordingly to your screens)
###rv
Launches a random video from viddir (non recursive)
###tg
Shortcut to launch tilda with specific configuration
###tn
Shortcut to rename current tilda tab
###lockit
lock the screen
###zmount
mounts a zfs dataset passed as $1, asks for key if it is encrypted
